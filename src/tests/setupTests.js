import DotEnv from 'dotenv';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TestUtils from 'react-dom/test-utils';

Enzyme.configure({
  adapter: new Adapter()
});

DotEnv.config({ path: '.env.test' });
