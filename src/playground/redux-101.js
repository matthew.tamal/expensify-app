import { createStore } from 'redux';

// const add = ({ a, b }, c) => {
//   return a + b + c;
// };
//
// console.log(add({ a:1, b:2 }, 100));

// Action Generators
const incrementBy = ({ incrementBy = 1} = {}) => ({
  type: 'INCREMENT',
  incrementBy
});

const decrementBy = ({ decrementBy = 1 } = {}) => ({
  type: 'DECREMENT',
  decrementBy
});

const setCount = ({ count }) => ({
  type: 'SET',
  count
});

const resetCount = () => ({
  type: 'RESET'
});

// Reducers
// 1. Reducers are pure functions (only use parameters to compute)
// 2. Never change state or action
const countReducer = (state = { count: 0 }, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        count: state.count + action.incrementBy
      };
    case 'DECREMENT':
      return {
        count: state.count - action.decrementBy
      };
    case 'SET':
      return {
        count: action.count
      }
    case 'RESET':
      return {
        count: 0
      };
    default:
      return state;
  }
}

const store = createStore(countReducer);

const unsubscribe = store.subscribe(() => {
  console.log(store.getState());
});

// Actions - an object that gets sent to the store
// Increment
store.dispatch(incrementBy({ incrementBy: 5 }));

store.dispatch(incrementBy());

// Reset
store.dispatch(resetCount());

// Decrement
store.dispatch(decrementBy());

store.dispatch(decrementBy({ decrementBy: 10 }));

// Set
store.dispatch(setCount({ count: 101 }));
