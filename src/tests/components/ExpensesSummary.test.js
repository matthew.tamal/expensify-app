import React from 'react';
import { shallow } from 'enzyme';
import { ExpensesSummary } from '../../components/ExpensesSummary';
// import expenses from '../fixtures/expenses';
// import { filters } from '../fixtures/filters';
// import selectExpenses from '../../selectors/expenses';
// import selectExpensesTotal from '../../selectors/expenses-total';
//
// test('should render ExpensesSummary component', () => {
//   const expenseCount = selectExpenses(expenses, filters).length;
//   const expensesTotal = selectExpensesTotal(expenses);
//   const wrapper = shallow(
//     <ExpensesSummary
//       expenseCount={expenseCount}
//       expensesTotal={expensesTotal}
//     />
//   );
//   expect(wrapper).toMatchSnapshot();
// });
//
// test('should show nothing when 0 expenses', () => {
//   const expenseCount = selectExpenses([], filters).length;
//   const expensesTotal = selectExpensesTotal([]);
//   const wrapper = shallow(
//     <ExpensesSummary
//       expenseCount={expenseCount}
//       expensesTotal={expensesTotal}
//     />
//   );
//   expect(wrapper).toMatchSnapshot();
// });

test('should correctly render ExpensesSummary with 1 expense', () => {
  const wrapper = shallow(<ExpensesSummary expenseCount={1} expensesTotal={235}/>)
  expect(wrapper).toMatchSnapshot();
});

test('should correctly render ExpensesSummary with multiple expenses', () => {
  const wrapper = shallow(<ExpensesSummary expenseCount={23} expensesTotal={23512340987}/>)
  expect(wrapper).toMatchSnapshot();
});
