import expensesImport from '../tests/fixtures/expenses';
import * as firebase from 'firebase';


const config = {
    apiKey: "AIzaSyDe9fm_cTx-aRxsJH4rQZKlhwUlxeWMlSw",
    authDomain: "expensify-8e95d.firebaseapp.com",
    databaseURL: "https://expensify-8e95d.firebaseio.com",
    projectId: "expensify-8e95d",
    storageBucket: "expensify-8e95d.appspot.com",
    messagingSenderId: "974054800789"
  };

firebase.initializeApp(config);

const database = firebase.database();

// const expenseRemoved = database.ref('expenses').on('child_removed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });
//
// const expenseChanged = database.ref('expenses')
//   .on('child_changed', (snapshot) => {
//     console.log(snapshot.key, snapshot.val());
//   });
//
// const expenseAdded = database.ref('expenses')
//   .on('child_added', (snapshot) => {
//     console.log(snapshot.key, snapshot.val());
//   });

// database.ref('expenses')
//   .once('value')
//   .then((snapshot) => {
//     const expenses = [];
//     snapshot.forEach((childSnapshot) => {
//       expenses.push({
//         id: childSnapshot.key,
//         ...childSnapshot.val()
//       });
//     });
//     console.log(expenses);
//   });

// const onExpensesChange = database.ref('expenses').on('value', (snapshot) => {
//   const expenses = [];
//   snapshot.forEach((childSnapshot) => {
//     expenses.push({
//       id: childSnapshot.key,
//       ...childSnapshot.val()
//     });
//   });
//   console.log(expenses);
// }, (e) => {
//   console.log('Error retrieving array data: ', e);
// });

// expensesImport.forEach(({ description, note, amount, createdAt }) => {
//   database.ref('expenses').push({
//     description, note, amount, createdAt
//   });
// });

// database.ref('notes/-LCBpHeDx5igeQl3nNKz').update({
//   body: 'Finish this course'
// });

// database.ref('notes').push({
//   title: 'Course topics',
//   body: 'React, Angular, Native'
// });

// const firebaseNotes = {
//   notes: {
//     arosteart: {
//       title: 'First note!',
//       body: 'This is my note'
//     },
//     rsaitnarst: {
//       title: 'Another note',
//       body: 'This is my note'
//     }
//   }
// }
//
// const notes = [{
//   id: '12',
//   title: 'First note!',
//   body: 'This is my note'
// }, {
//   id: '424',
//   title: 'Another note',
//   body: 'This is my note'
// }];
//
// database.ref('notes').set(notes);



// const onDataChange = database.ref().on('value', (snapshot) => {
//   const { name, job } = snapshot.val();
//   console.log(`${name} is a ${job.title} at ${job.company}.`);
// }, (e) => {
//   console.log('Data sub error:', e);
// });


// const onValueChange = database.ref().on('value', (snapshot) => {
//   console.log(snapshot.val());
// }, (e) => {
//   console.log('Error with data fetching', e);
// });
//
// setTimeout(() => {
//   database.ref('age').set(27);
// }, 3500);
//
// setTimeout(() => {
//   database.ref().off(onValueChange);
// }, 7000);
// setTimeout(() => {
//   database.ref('age').set(30);
// }, 10500);



// database.ref('location/city')
//   .once('value')
//   .then((snapshot) => {
//     const val = snapshot.val();
//     console.log(val);
//   })
//   .catch((e) => {
//     console.log('Error fetching data', e);
//   });


// database.ref().set({
//   name: 'Matt Tamal',
//   age: 26,
//   stressLevel: 6,
//   job: {
//     title: 'Software Developer',
//     company: 'Google'
//   },
//   location: {
//     city: 'Pickering',
//     country: 'Canada'
//   }
// }).then(() => {
//   console.log('Data is saved');
// }).catch((e) => {
//   console.log('This failed.', e)
// });
//
// database.ref().update({
//   stressLevel: 9,
//   'job/company': 'Amazon',
//   'location/city': 'Seattle',
// });

// database.ref()
// .remove()
// .then(() => {
//   console.log('Data was removed');
// })
// .catch((e) => {
//   console.log('Data remove error', e);
// });
